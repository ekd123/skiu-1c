# Rooms

The word, "room", stands for a place to talk with others easily. 

A room is very similar to a room in IRC.

Room is owned by nobody.

## Join a new room

{
    t: JOIN_ROOM
    n: roomname
}

This is also used to create a room.

## Quit a room

{
    t: QUIT_ROOM
    n: roomname
}
