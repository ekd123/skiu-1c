# The SKiu Protocol

The SKiu Protocol takes the advantages of a binary standard and avoids the complexity
of [XMPP](http://www.xmpp.org). It's also more powerful than IRC. This protocol aims 
to be the most feature-rich IM protocol.

SKiu is effective, fast, yet extensible. We prefer [BSON](http://www.bsonspec.org) 
over XML.

**NOTE**: This is not the standard but a document for server and client developers.

## SKiu Versions

'\x00\x00' is always invalid.

* '\x1E\x0D', development started in 2013

Every connection should begin with '\x02'.

