# Register

Register is done by the core. [`libskiu`](https://github.com/ekd123/libskiu) 
provides a well-structured API.


    {
        t: NEW_ACCOUNT,
        u: id,
        p: password,
        n: nickname
    }
