# Status

The word, "state", stands for presence in XMPP, though ours has fewer features.

Acceptable status:

* `online` - this user is normally on line, his buddies will see him in their buddy 
  lists
* `away` - this user is away, his buddies will see him, but when buddies send him
  messages, the server will automatically replies "this person is not here".
* `busy` - this user is busy, his buddies will see him.
* `talktome` - this user **hopes** to talk. the client is expected to show up at once 
  when one buddy or group sends him messages.
* `invisible` - nobody can see him.
* `offline` - this user hasn't logged in.

The text of a standard state may be changed. This kind of text is so-called personal
signature.

## Change your state

The basic BSON request is: 

	t (i) STATUS_GO*				# t -> type
	s (s) (Chosen by the server)	# s -> signature

The full list is:
* `STATUS_GOONLINE`: be `online`, if the user hasn't signed in yet, this is the log in.
  Value: 100.
* `STATUS_GOAWAY`: be `away`. Value: 101.
* `STATUS_GOBUSY`: be `busy`. Value: 102.
* `STATUS_GOTALKTOME`: be `talk`. Value: 103.
* `STATUS_GOINVISIBLE`: be `invisible`. Value: 104.
* `STATUS_GOOFFLINE`: disconnect. Value: 105.

Some requests need to provide more information, like "GOONLINE".

### From offline to online

This is the "log in", which requires username and password. The server may detect the 
unusual activies of one user, and send him a CAPTCHA picture, and wait for a response
for one minute.

A request, for example, from the client side:

	t: STATUS_GOONLINE		# t -> type
	u: "username/account"	# u -> user
	p: "password"			# p -> password

If the server wants to send a captcha picture:

	t: STATUS_GOONLINE
	d: raw data of the repatcha 	# d -> data

Client's response:

	t: STATUS_GOONLINE
	u: user
	p: password
	r: response, another doc, with key and contents

If everything goes well, the server will give the client:

	t: STATUS\_GOONLINE

### Go offline
Just disconnect from the server.

