# Talk

## Features

* Many beautiful and funny emoticons. (The same as Photo sharing)
* Great animation to show your mood.
* Choose fonts, colors and sizes. (client-only)
* Photo sharing on the fly.
* Source code hilighting. (client-only)
* File transferring. (P2P)
* Remotely helping.
* Audio/Video.

We are more powerful than XMPP and IRC!

## Messaging

'e' is an array of binary data. Use this to implement photo sharing.

a message body is a UTF8-encoded string, or with Pango markup.

### sending

{
    t: MESSAGE
    w: "to whom"
    m: "message body"
    e: "extra data"
}

### receiving

{
    t: MESSAGE
    f: "from whom"
    m: "message body"
    e: "extra data"
    (s: time, only if this message is sent while user is offline)
    (r: "room name", if it's sent from a room)
    (g: "group name", if it's sent from a group)
}

Due to an implementation detail, 'w' may be seen here, just ignore it.
