# Groups

A group is similar to a group in QQ.

Group is created by somebody and it's owned by the same person. He is the "root" of
this group and he can decide to transfer the ownership to others. One group may
have at most 5 admins.

A group can have a password to stop others joining it.

