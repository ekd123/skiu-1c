# Monitor

All buddies and joined groups are automatically monitored.
This is mainly used for notifying state changing.

Server-to-client one-way.

A notification:

    {
        t: MONITOR, 
        u: id, 
        s: status, 
        t: signature
    }
