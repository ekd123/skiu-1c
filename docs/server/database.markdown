# Database

The implementation details are not covered and should not be covered here.
Because of this strategy, you may freely choose a better way to do what you want.

Current reference implementation is written by Mike Manilone, with SQLite3.

## User

Required fields.

* Username (id)
* Checksum of password+salt
* Salt

Even more user friendly, and required:

* Nickname
* Buddy list
* Joined group list
* Messages, which are sent to an offline user, and the messages are resent to 
him as if he logged in.

### Buddy list

Based on XML.

<group name="Classmates">
    <group name="Primary school">
        <person id="John Doe" />
    </group>
    <person id="a" />
</group>

### Logs

* fromid
* message (the original document)
* time

## Group

* creator
* admins
* members
* logs
