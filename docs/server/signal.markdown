# Signal

The signal system is in client.c. It mostly simulates 
one which GObject has.

Because this system is only used by a few parts of SKiu,
I didn't make it a standalone part.