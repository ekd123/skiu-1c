# SKiu

SKiu is a server targetting the brand new SKiu Protocol for instant messaging.

SKiu is expected to run on all the POSIX systems.

# Dependencies
* libevent (3-clause BSD License)
* gio (LGPL)
* libbson (Apache License 2.0)

Install the dependencies on a FreeBSD platform:

    portmaster devel/glib20 devel/libevent2

Install the dependencies on a Debian platform:

    apt-get install libglib2.0-dev libevent-dev

Install the dependencies on a RedHat platform:

    yum install glib-devel libevent-devel

You need to install libbson manually on all these three platforms.

## PostgreSQL Support

This needs libpq (Apache License 2.0).

## SQLite3 Support

This needs sqlite3 (Public domain).

# Compile

Firstly, modify `src/config.h` as you like.

    cd src
    make 

# Begin to serve

    sudo ./skiu

# Support

FreeBSD and Linux is supported officially by the team.

We offer commercial support for the both platforms. 
Contact Mike Manilone for more details.

# License

This software is protected by Restricted BSD License 1.0 with some exceptions.

