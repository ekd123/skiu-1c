#!/usr/bin/env python3

## 
## Test Suite: Connect with the bad version and get the error report
## 

import socket
from bson import BSON

host = socket.gethostname()
port = 12345
s = socket.socket()
s.connect((host,port))
s.send(b"\x02\x1E\x00")
bson_string = BSON.decode(s.recv(10000))
if bson_string['t'] == 500:
	print("Errno: %s, Description: %s" % (bson_string['n'], bson_string['d']))
else:
	print("This shouldn't happen...")
s.close 

