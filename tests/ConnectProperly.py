#!/usr/bin/env python3

## 
## Test Suite: Connect to the server properly and then disconnect
## 

import socket
from time import sleep
host = socket.gethostname()
port = 12345
s = socket.socket()
s.connect((host,port))
s.send(b"\x02\x1E\x0D")
s.close 

