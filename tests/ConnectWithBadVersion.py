#!/usr/bin/env python3

##
## Test Suite: connect with a bad version and then disconnect
##

import socket
host = socket.gethostname()
port = 12345
s = socket.socket()
s.connect((host,port))
s.send(b"\x02\x00\x00")
s.close 
