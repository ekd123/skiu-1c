#!/usr/bin/env python3

## 
## Test Suite: Connect properly and get the notification of state changing
## 

import socket
from bson import BSON

host = socket.gethostname()
port = 12345
s = socket.socket()
s.connect((host,port))
s.send(b"\x02\x1E\x0D")
bson_string = BSON.decode(s.recv(10000))
print(bson_string)
s.close 

