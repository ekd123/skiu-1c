#!/usr/bin/env python

## 
## Test Suite: Simple connecting and disconnecting...
## 

import socket
host = socket.gethostname()
port = 12345

while True:
    s = socket.socket()
    s.connect((host,port))
    s.close

