#pragma once

#include "database.h"

/**
 * database_sqlite3_new:
 * Return value: (transfer-full): a new #idatabase for SQLite3.
 */
idatabase *database_sqlite3_new (void);
