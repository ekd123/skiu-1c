#include <glib.h>
#include "log.h"
#include "user.h"
#include "client.h"
#include "server.h"

/**
 * we have some "rubbish" code here.
 * it's thrown here because it's too ugly to put it back.
 */

/* server.c */
void _close_cb (gpointer data)
{
    LOG_debug ("Close client on 0x%lX", (unsigned long)data);
    protocol_send_error (((SkClient*)data)->fd, ERROR_SERVER_QUIT, "Server has "
        "quited.");
    client_free ((SkClient*)data);
}

/* server.c */
void _client_destroyed_cb (SkClient *client, gpointer user_data)
{
    server_listener_remove (client);
    user_mgmt_offline (client);
}

/* room.c */
void _send_cb (gpointer data, gpointer user_data)
{
    client_send_message ((SkClient *)data, (bson_t *)user_data);
}
