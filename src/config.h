#pragma once

#include <glib.h>

#define SERVER_NAME "skiu"
#define SERVER_BACKLOG 10000
#define SERVER_MAXEVENTS SERVER_BACKLOG

G_BEGIN_DECLS

gchar *config_get_preferred_db (void);
gchar *config_get_pgsql_conninfo (void);
gchar *config_get_sqlite_path (void);
gchar *config_get_pid_path (void);

gint config_get_port (void);

G_END_DECLS