#pragma once

#include <stdbool.h>
#include <gio/gio.h>
#include <bson.h>
#include <event.h>

#include "protocol.h"

typedef enum {
    USER_NONEXIST,
    USER_OFFLINE,
    USER_ONLINE
} SkUserState;

/**
 * SkClient:
 * @socket: Socket obj
 * @fd: Socket fd. the same as this.socket.get_fd().
 * @version: Client version.
 * @ev_read: struct event.
 * @destroysignal: emitted when the object is being finalizing
 * @status: current state number
 * @loggedin: 
 * @assd_id: associated id
 * structure that represents a client connection.
 */
typedef struct
{
    GSocket *socket;
    int fd;
    char version[2];
    struct event ev_read;
    GHookList destroysignal;

    SkMessageType status;
    bool loggedin;
    char *assd_id;
} SkClient;

/**
 * client_new_empty:
 * @fd: fd
 * Create an empty SkClient for @fd.
 */
SkClient *client_new_empty (int fd);
/**
 * client_new:
 * @fd: fd.
 * Create a SkClient for @fd.
 * Return value: (transfer-full): a #SkClient
 */
SkClient *client_new (int fd);
/**
 * client_free:
 * @self: a #SkClient
 * 
 * Free a #SkClient.
 */
void client_free (SkClient *self);
/**
 * client_send_message:
 * @client: 'from'
 * @doc: the doc which holds message
 * 
 * Send a message from @client according to @doc.
 */
void client_send_message (SkClient *client, const bson_t *doc);
/**
 * client_message_handle:
 * @client: a #SkClient
 * @doc: a bson_t
 * 
 * Handle a valid message.
 */
void client_message_handle (SkClient *client, const bson_t *doc);
/**
 * client_status:
 * @id: id to be checked
 * @client: (out): get the client
 * 
 * Get client status.
 */
SkUserState client_status (const char *id, const SkClient ** client);

/**
 * client_connect_destroy:
 * @client: a #SkClient
 * @func: function to call when the signal is emitted
 * @user_data: user data
 * 
 * Connect to the 'destroy' signal.
 */
void client_connect_destroy (SkClient *client, GFunc func, 
    gpointer user_data);
