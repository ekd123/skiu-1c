#pragma once

/**
 * random_generate_salt:
 * @length: length of the salt you want
 * 
 * Generate a random string.
 * 
 * Return value: (transfer-full): a newly-allocated string, with NUL. 
 * length=@length+1.
 */
gchar *random_generate_salt (size_t length);
