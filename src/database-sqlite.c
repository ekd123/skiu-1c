#include <glib.h>
#include <sqlite3.h>
#include "config.h"
#include "log.h"
#include "random.h"
#include "database.h"

/**
 * TABLE: users
 * * ID
 * * NICKNAME
 * * PASSWORD (checksum)
 * * SALT
 * * [TODO] BUDDIES (list of IDs)
 * * [TODO] GROUPS (list of IDs)
 * * [TODO] INCOMING (what others sent to this user, but the user isn't online)
 * 
 * TABLE: logs
 * * toid
 * * doc
 * * date
 */

#define ERROR_QUARK g_quark_from_static_string("database-sqlite")

struct priv 
{
    sqlite3 *db;
};

static void _free (idatabase *i)
{
    struct priv *priv;
    priv = (struct priv *)i->extra_data;
    sqlite3_close (priv->db);
    g_free (priv);
    g_free (i);
}

static void _connect (idatabase *i)
{
    struct priv *priv;
    gchar *sqlite_path = config_get_sqlite_path ();
    priv = (struct priv *)i->extra_data;
    sqlite3_open (sqlite_path, &priv->db);
    sqlite3_exec (priv->db, "CREATE TABLE users (id TEXT PRIMARY KEY, "
        "nickname TEXT, password CHAR(128), salt CHAR(10));", NULL, NULL, NULL);
    sqlite3_exec (priv->db, "CREATE TABLE logs (toid TEXT, doc TEXT, "
        "date INT64)", NULL, NULL, NULL);
    g_free (sqlite_path);
}

static void _disconnect (idatabase *i)
{
}

static bool _auth (idatabase *i, const char *id, const char *password, 
    SkClient *client)
{
    struct priv *priv = (struct priv *)i->extra_data;
    char *sql = NULL;
    sqlite3_stmt *stmt;
    const guchar *stored_password, *stored_salt;
    bool matched = false;
    
    if (database_valid (id) != true)
        return false;
    sql = g_strdup_printf ("SELECT * FROM users WHERE id=\"%s\"", id);
    if (sqlite3_prepare_v2 (priv->db, sql, -1, &stmt, NULL) != SQLITE_OK)
    {
        LOG_critical ("Query failed: %s", sqlite3_errmsg (priv->db));
        return false;
    }
    while (sqlite3_step (stmt) == SQLITE_ROW)
    {
        gchar *tmp;
        gchar *computed_password = NULL;
        
        stored_password = sqlite3_column_text (stmt, 2);
        stored_salt = sqlite3_column_text (stmt, 3);
        tmp = g_strdup_printf ("%s%s%s", stored_salt, password, stored_salt);
        computed_password = g_compute_checksum_for_string (G_CHECKSUM_SHA512, 
            tmp, -1);
        if (!g_strcmp0 (computed_password, (const char*)stored_password))
            matched = true;
        g_free (tmp);
        g_free (computed_password);
    }
    sqlite3_finalize (stmt);
    return matched;
}

static SkErrorNumber 
_register (idatabase *i, const char *id, const char *nickname, 
    const char *password, GError **error)
{
    if (database_if_register (id) == true)
    {
        g_set_error_literal (error, ERROR_QUARK, ERROR_ID_TAKEN, 
            "This ID has already been taken");
        return false;
    }
    
    bool success = true;
    struct priv *priv = (struct priv *)i->extra_data;
    gchar *salt = random_generate_salt (10);
    gchar *combined = g_strdup_printf ("%s%s%s", salt, password, salt);
    gchar *checksum = g_compute_checksum_for_string (G_CHECKSUM_SHA512, 
        combined, -1);
    gchar *sql = g_strdup_printf ("INSERT INTO users VALUES (\"%s\", \"%s\", "
        "\"%s\", \"%s\")", id, nickname, checksum, salt);
    char *errmsg = NULL;
    if (sqlite3_exec (priv->db, sql, NULL, NULL, &errmsg) != SQLITE_OK)
    {
        LOG_critical ("Couldn't insert values: %s", errmsg);
        free (errmsg);
        success = false;
        g_set_error_literal (error, ERROR_QUARK, ERROR_INTERNAL, 
            "Server internal error");
    }
    g_free (salt);
    g_free (combined);
    g_free (checksum);
    g_free (sql);
    if (success)
    {
        LOG_message ("new account ID: %s, Nick: %s", id, nickname);
    }
    return success;
}

static bool _if_register (idatabase *i, const char *id)
{
    struct priv *priv = (struct priv *)i->extra_data;
    char *sql = NULL;
    sqlite3_stmt *stmt;
    bool found = false;
    
    if (database_valid (id) != true)
        return false;
    sql = g_strdup_printf ("SELECT * FROM users WHERE id=\"%s\"", id);
    if (sqlite3_prepare_v2 (priv->db, sql, -1, &stmt, NULL) != SQLITE_OK)
    {
        LOG_critical ("Query failed: %s", sqlite3_errmsg (priv->db));
        return false;
    }
    while (sqlite3_step (stmt) == SQLITE_ROW)
    {
        const guchar *gotid = sqlite3_column_text (stmt, 0);
        if (!g_strcmp0 ((const char *)gotid, id))
            found = true;
    }
    g_free (sql);
    sqlite3_finalize (stmt);
    return found;
}

static bool _log (idatabase *i,const char *toid, const bson_t *doc, GDateTime *utctime)
{
    bool success = true;
    struct priv *priv = (struct priv *)i->extra_data;
    char *sql = g_strdup_printf ("INSERT INTO logs VALUES ('%s',?,'%ld')",
        toid, g_date_time_to_unix (utctime));
    sqlite3_stmt *stmt;
    if (sqlite3_prepare_v2 (priv->db, sql, -1, &stmt, NULL) != SQLITE_OK)
    {
        LOG_critical ("couldn't log offline messages: %s", 
            sqlite3_errmsg (priv->db));
        g_free (sql);
        return FALSE;
    }
    g_free (sql);
    if (sqlite3_bind_blob (stmt, 1, bson_get_data (doc), doc->len, 
        SQLITE_STATIC) != SQLITE_OK)
    {
        LOG_critical ("couldn't bind blob: %s", sqlite3_errmsg (priv->db));
        success = false;
    }
    if (sqlite3_step (stmt) != SQLITE_DONE)
    {
        LOG_critical ("couldn't log offline messages: %s", 
            sqlite3_errmsg (priv->db));
        success = false;
    }
    sqlite3_finalize (stmt);
    return success;
}

static GList *_get_logs (struct idatabase *i, const char *toid)
{
    GList *list = NULL;
    struct priv *priv = (struct priv *)i->extra_data;
    char *sql;
    sqlite3_stmt *stmt;
    
    /* Read messages */
    sql  = g_strdup_printf ("SELECT * FROM logs WHERE toid='%s'", toid);
    if (sqlite3_prepare_v2 (priv->db, sql, -1, &stmt, NULL) != SQLITE_OK)
    {
        LOG_critical ("couldn't select from logs: %s", 
            sqlite3_errmsg (priv->db));
        g_free (sql);
        return NULL;
    }
    g_free (sql);
    while (sqlite3_step (stmt) == SQLITE_ROW)
    {
        const void *odoc = (const void *)sqlite3_column_blob (stmt, 1);
        const gint64 time = sqlite3_column_int64 (stmt, 2);
        
        bson_t *doc = bson_new_from_data (odoc, sqlite3_column_bytes (stmt, 1));
        bson_append_time_t (doc, "s", 1, time);
        list = g_list_append (list, doc);
    }
    sqlite3_finalize (stmt);
    /* Delete messages */
    sql = g_strdup_printf ("DELETE FROM logs WHERE toid='%s'", toid);
    if (sqlite3_prepare_v2 (priv->db, sql, -1, &stmt, NULL) != SQLITE_OK)
    {
        LOG_critical ("couldn't select from logs: %s", 
            sqlite3_errmsg (priv->db));
        g_free (sql);
        return NULL;
    }
    g_free (sql);
    if (sqlite3_step (stmt) != SQLITE_DONE)
    {
        LOG_critical ("Couldn't delete offline messages: %s", 
            sqlite3_errmsg (priv->db));
    }
    sqlite3_finalize (stmt);
    return list;
}

idatabase *database_sqlite3_new (void)
{
    struct priv *priv = g_malloc0 (sizeof (struct priv));
    idatabase *i = (idatabase*)g_malloc0 (sizeof (idatabase));
    i->free = _free;
    i->dbconnect = _connect;
    i->dbdisconnect = _disconnect;
    i->auth = _auth;
    i->new_account = _register;
    i->if_register = _if_register;
    i->log = _log;
    i->get_logs = _get_logs;
    i->extra_data = priv;
    return i;
}
