#pragma once

#include <stdbool.h>
#include <glib.h>
#include <bson.h>
#include "client.h"
#include "user.h"

/**
 * idatabase:
 * @dbconnect: connect to the database
 * @dbdisconnect: disconnect to the database
 * @free: free #idatabase itself
 * @extra_data: some extra data...
 * @auth: auth the user and then returns a #SkAccount.
 * 
 * An interface.
 */
typedef struct idatabase {
    void (*dbconnect)(struct idatabase *);
    void (*dbdisconnect)(struct idatabase *);
    void (*free)(struct idatabase *);
    gpointer extra_data;
    /* user */
    bool (*auth)(struct idatabase *, const char *id, const char *password,
        SkClient *client);
    SkErrorNumber (*new_account)(struct idatabase *, const char *, const char *,
        const char *, GError **);
    bool (*if_register)(struct idatabase *, const char *id);
    /* log */
    bool (*log)(struct idatabase *, const char *toid, const bson_t *doc, 
        GDateTime *utctime);
    GList *(*get_logs)(struct idatabase *,const char *toid);
} idatabase;

/**
 * database_get_preferred:
 * 
 * gets the preferred interface.
 * 
 * Return value: (transfer-none): the preferred #idatabase. This should only be 
 * freed by the database system itself.
 **/
idatabase *database_get_preferred (void);
/**
 * database_ifree:
 * @i: a #idatabase
 * Free @i.
 */
void database_ifree (idatabase *i);
/**
 * database_init:
 * Initialize the database system.
 */
void database_init (void);
/**
 * database_uninit:
 * Uninitialize the database system.
 */
void database_uninit (void);
/**
 * database_auth:
 * Auth a user and (TODO) add him to the online-user list.
 * Return value: true if auth'd successfully.
 */
bool database_auth (const char *id, const char *password, SkClient *client);
/**
 * database_valid:
 * @request: a SQL entry
 * 
 * validate the an column/entry, like username.
 * 
 * Return value: true if valid
 */
bool database_valid (const char *request);

/**
 * database_new_account:
 * @id: ID to be registered
 * @nickname: nickname to be taken
 * @password: password
 * @error: #GError for error reporting
 * 
 * Register a new user.
 * 
 * Return value: true if success
 */
bool database_new_account (const char *id, const char *nickname, 
    const char *password, GError **error);

/**
 * database_if_register:
 * @id: id to be checked
 * 
 * Check if @id is registered.
 * 
 * Return value: true or false.
 */
bool database_if_register (const char *id);

/**
 * database_log:
 * @fromid: who sent this
 * @doc: the original bson_t
 * @utctime: when @doc is sent, in UTC time
 * 
 * Log a message but the dest user is offline.
 * 
 * Return value: true or false.
 */
bool database_log (const char *toid, const bson_t *doc, GDateTime *utctime);
bool database_log_now (const char *toid, const bson_t *doc);

/**
 * database_get_logs:
 * @toid: whose offline messages
 * 
 * Get offline messages.
 * 
 * Return value: a list of bson_t.
 */
GList *database_get_logs (const char *toid);
