#pragma once

#define CONSTRUCTOR(p) __attribute__((constructor(p)))
#define DESTRUCTOR(p) __attribute__((destructor(p)))

/**
 * setnonblock:
 * @fd: fd to be set nonblocking.
 * 
 * Set @fd as non-blocking.
 * 
 * Return value: the result. 0 on success.
 */
int setnonblock(int fd);
