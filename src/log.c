#include <syslog.h>
#include <stdlib.h>
#include <glib.h>
#include "config.h"
#include "common.h"
#include "log.h"

#ifndef DEBUG
static void _log_func (const gchar *log_domain, GLogLevelFlags log_level, 
    const gchar *message, gpointer user_data)
{
    int priority;
    if (log_level & G_LOG_LEVEL_ERROR)
        priority = LOG_ERR;
    else if (log_level & G_LOG_LEVEL_CRITICAL)
        priority = LOG_CRIT;
    else if (log_level & G_LOG_LEVEL_WARNING)
        priority = LOG_WARNING;
    else if (log_level & G_LOG_LEVEL_MESSAGE)
        priority = LOG_INFO;
    else if (log_level & G_LOG_LEVEL_INFO)
        priority = LOG_INFO;
    else if (log_level & G_LOG_LEVEL_DEBUG)
        priority = LOG_DEBUG;
    else 
    {
        g_assert_not_reached ();
    }
    syslog (priority, "%s", message);
}
#endif 

CONSTRUCTOR(101)
void log_init (void)
{
#ifndef DEBUG
    openlog ("skiu", LOG_NDELAY|LOG_CONS, LOG_DAEMON);
    g_log_set_default_handler (_log_func, NULL);
#endif
    LOG_debug ("log init'd");
}

DESTRUCTOR(101)
void log_uninit (void)
{
#ifndef DEBUG
    closelog ();
#endif
    LOG_debug ("log uninit'd");
}
