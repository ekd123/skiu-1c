#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <event.h>
#include <glib.h>
#include <gio/gio.h>
#include <bson.h>
#include "config.h"
#include "log.h"
#include "common.h"
#include "client.h"
#include "user.h"
#include "server.h"

static GList *clients = NULL;
static struct event_base *evbase = NULL;

CONSTRUCTOR(140)
void server_init (void)
{
#ifdef DEBUG
    event_enable_debug_mode ();
#endif
    evbase = event_base_new ();
    LOG_debug ("server init'd");
}

static void on_client_read_cb (int fd, short ev, gpointer user_data)
{
    SkClient *client = (SkClient *)user_data;
    bson_reader_t reader;
    const bson_t *doc;
    bson_bool_t reached_eof = FALSE;
    
    bson_reader_init_from_fd (&reader, client->fd, false);
    doc = bson_reader_read (&reader, &reached_eof);
    if (reached_eof || !doc)
    {
        /* Client has disconnected */
        client_free (client);
        goto cleanup;
    }
    /* continue to work */
    LOG_debug ("valid doc received from %d", fd);
    client_message_handle (client, doc);
  cleanup:
    bson_reader_destroy (&reader);
}

static void on_accept_cb (int fd, short ev, gpointer user_data)
{
    int infd;
    struct sockaddr_in client_addr;
    socklen_t client_len;
    SkClient *client;

    client_len = sizeof(client_addr);
    infd = accept(fd, (struct sockaddr *)&client_addr, &client_len);
    if (infd == -1)
    {
        LOG_warning ("one connection lost");
        return;
    }
    if (setnonblock (infd) < 0)
    {
        LOG_warning ("fd %d couldn't be set nonblocking, closing", infd);
        close (infd);
        return;
    }
    LOG_message ("new connection with fd %d accepted", infd);
    client = client_new (infd);
    if (client)
    {
        extern void _client_destroyed_cb (SkClient *client, gpointer user_data);
        server_listener_add (client);
        client_connect_destroy (client, (GFunc)_client_destroyed_cb, NULL);
    }
}

void server_listener_start (void)
{
    int listen_fd;
    struct sockaddr_in listen_addr;
    int reuseaddr_on = 1;
    struct event event = {};

    listen_fd = socket (AF_INET, SOCK_STREAM, 0);
    if (listen_fd < 0)
    {
        LOG_error ("listen fd failed");
        abort ();
    }
    if (setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr_on,
        sizeof(reuseaddr_on)) == -1)
    {
        LOG_error ("setsockopt failed");
        abort ();
    }
    memset(&listen_addr, 0, sizeof(listen_addr));
    listen_addr.sin_family = AF_INET;
    listen_addr.sin_addr.s_addr = INADDR_ANY;
    listen_addr.sin_port = htons(config_get_port ());
    if (bind(listen_fd, (struct sockaddr *)&listen_addr, sizeof(listen_addr))
        < 0)
    {
        LOG_error ("bind failed");
        abort ();
    }
    if (listen (listen_fd, SOMAXCONN) < 0)
    {
        LOG_error ("listen failed");
        abort ();
    }
    if (setnonblock (listen_fd) < 0)
    {
        LOG_error ("failed to set the non-blocking flag");
        abort ();
    }
    event_assign (&event, evbase, listen_fd, EV_READ|EV_PERSIST, on_accept_cb, NULL);
    event_add (&event, NULL);

    LOG_message ("Listener starting.");
    event_base_dispatch (evbase);
    LOG_message ("Listener stopped.");
}

void server_listener_add (SkClient *client)
{
    clients = g_list_append (clients, client);
    event_assign (&client->ev_read, evbase, client->fd, EV_READ|EV_PERSIST, 
        on_client_read_cb, client);
    event_add (&client->ev_read, NULL);
    protocol_confirm_change_status (client->fd, STATUS_GOOFFLINE);
    LOG_debug ("clientfd %d added", client->fd);
}

void server_listener_remove (SkClient *client)
{
    event_del (&client->ev_read);
    clients = g_list_remove (clients, client);
    LOG_debug ("clientfd %d removed", client->fd);
}

void server_close_all (void)
{
    extern void _close_cb (gpointer data);
    g_list_free_full (clients, _close_cb);
    clients = NULL;
}

DESTRUCTOR(140)
void server_uninit (void)
{
    event_base_loopbreak (evbase);
    event_base_free (evbase);
    server_close_all (); /* also frees `clients' */
    LOG_debug ("server uninit'd");
}
