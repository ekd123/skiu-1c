#include <glib.h>
#include "config.h"
#include "common.h"
#include "log.h"
#include "database.h"
#include "database-pgsql.h"
#include "database-sqlite.h"

idatabase *database_get_preferred (void)
{
    static idatabase * interface = NULL;
    gchar *preferred_db = config_get_preferred_db ();
    if (interface == NULL)
    {
        if (!g_strcmp0 (preferred_db, "sqlite"))
        {
            interface = database_sqlite3_new ();
        }
        else if (!g_strcmp0 (preferred_db, "postgresql"))
        {
            interface = database_pgsql_new ();
        }
        else 
        {
            LOG_error ("No such database support.");
            abort ();
        }
    }
    g_free (preferred_db);
    return interface;
}

CONSTRUCTOR(120)
void database_init (void)
{
    database_get_preferred()->dbconnect (database_get_preferred());
    LOG_debug ("database init'd");
}

DESTRUCTOR(120)
void database_uninit (void)
{
    database_get_preferred()->dbdisconnect (database_get_preferred ());
    database_get_preferred()->free (database_get_preferred ());
    LOG_debug ("database uninit'd");
}

bool database_auth (const char *id, const char *password, SkClient *client)
{
    return database_get_preferred()->auth (database_get_preferred(), id, 
        password, client);
}

bool database_valid (const char *request)
{
    if (request == NULL)
        return false;
    if (g_strstr_len (request, -1, ";") != NULL)
        return false;
    else if (g_strstr_len (request, -1, "\"") != NULL)
        return false;
    else if (g_strstr_len (request, -1, "'") != NULL)
        return false;
    else if (g_strstr_len (request, -1, "DROP") != NULL)
        return false;
    else if (g_strstr_len (request, -1, "|") != NULL)
        return false;
    return true;
}

bool database_new_account (const char *id, const char *nickname, 
    const char *password, GError **error)
{
    return database_get_preferred ()->new_account (database_get_preferred (), 
        id, nickname, password, error);
}

bool database_if_register (const char *id)
{
    return database_get_preferred ()->if_register (database_get_preferred (), 
        id);
}

bool database_log (const char *toid, const bson_t *doc, 
    GDateTime *utctime)
{
    return database_get_preferred ()->log (database_get_preferred (), toid, doc,
        utctime);
}

bool database_log_now (const char *toid, const bson_t *doc)
{
    bool success;
    GDateTime * utctime = g_date_time_new_now_utc ();
    success = database_log (toid, doc, utctime);
    g_date_time_unref (utctime);
    return success;
}

GList *database_get_logs (const char *toid)
{
    return database_get_preferred ()->get_logs (database_get_preferred (), 
        toid);
}
