#pragma once

#include "client.h"

/**
 * server_init:
 * 
 * Initialize the listener.
 */
void server_init (void);

/** 
 * server_listener_start:
 * 
 * start the listener, and start handling client connections.
 **/
void server_listener_start (void);

/**
 * server_listener_add:
 * @client: a #SkClient
 * 
 * add @client to the monitoring list, and accept the future operations.
 **/
void server_listener_add (SkClient *client);

/**
 * server_listener_remove:
 * @client: a #SkClient
 * 
 * Remove @client from the monitoring list. If @client isn't here, do nothing.
 */
void server_listener_remove (SkClient *client);

/**
 * server_close_all:
 * 
 * Close all connections to all the clients.
 */
void server_close_all (void);

/**
 * server_uninit:
 * 
 * Uninitialize the server.
 */
void server_uninit (void);

