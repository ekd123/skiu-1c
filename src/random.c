#include <string.h>
#include <glib.h>
#include "random.h"

gchar *random_generate_salt (size_t length)
{
    char buf[length+1];
    memset (buf, 0, sizeof (buf));
    for (int i = 0; i < length; i++)
    {
        buf[i] = g_random_int_range ('a', 'z'+1);
    }
    return g_strdup (buf);
}
