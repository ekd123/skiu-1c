#pragma once

/**
 * This part covers user management and user representing.
 */

#include "client.h"
#include "protocol.h"

/**
 * user_mgmt_init:
 * 
 * Initialize the user management system.
 **/
void user_mgmt_init (void);

/**
 * user_mgmt_init:
 * 
 * Uninitialize the user management system.
 **/
void user_mgmt_uninit (void);

/**
 * user_mgmt_new_online:
 * @client: a #SkClient
 * 
 * Add @client to the 'online users' list.
 */
void user_mgmt_new_online (SkClient *client);

/**
 * user_mgmt_offline:
 * @client: a #SkClient
 * 
 * Remove @client from the 'online users' list. 
 */
void user_mgmt_offline (SkClient *client);

/**
 * user_mgmt_lookup:
 * See if one whose ID is @id is online, and return his #SkClient if he's 
 * online, or NULL is returned.
 */
const SkClient *user_mgmt_lookup (const char *id);
/**
 * user_map_new:
 * @writefd: fd for you to write
 * 
 * Create a new SkClient filled with info to 
 */
SkClient *user_map_new (GIOFunc read_ready);
