#include <stdlib.h>
#include <glib.h>
#include <glib/gi18n.h>
#include "pid.h"
#include "error.h"
#include "log.h"
#include "signal.h"
#include "database.h"
#include "security.h"
#include "server.h"
#include "room.h"

int main (int argc, char *argv[])
{
    setlocale (LC_ALL, "");

#ifndef DEBUG
    if (!pid_exists ())
    {
        pid_create ();
    }
    else 
    {
        gchar *pidstr;
        pidstr = pid_read ();
        LOG_error ("The server is already running (pid: %s)", pidstr);
        g_free (pidstr);
        return 1;
    }
    atexit (pid_delete);
#endif
    server_listener_start ();
    return EXIT_SUCCESS;
}
