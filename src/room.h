#pragma once

#include "client.h"

void room_init (void);
void room_uninit (void);

/**
 * room_join:
 * @roomname: room to join
 * @client: client
 * 
 * Join a room.
 * 
 * Return value: true or false.
 */
bool room_join (const gchar *roomname, const SkClient *client);

/**
 * room_quit:
 * @roomname: room to join
 * @client: client
 * 
 * Quit a room.
 * 
 * Return value: true or false.
 */
bool room_quit (const gchar *roomname, const SkClient *client);
