#include <string.h>
#include <glib.h>
#include <gio/gio.h>
#include "log.h"
#include "pid.h"

bool pid_exists (void)
{
    GFile *file;
    bool exists;
    gchar *pid_path;

    pid_path = config_get_pid_path ();
    file = g_file_new_for_path (pid_path);

    if (g_file_query_exists (file, NULL))
        exists = true;
    else
        exists = false;

    g_object_unref (file);
    g_free (pid_path);
    return exists;
}

bool pid_create (void)
{
    GFile *file;
    GFileIOStream *iostream;
    GOutputStream *output;
    GError *error;
    GCredentials *credentials;
    gchar *contents;
    gchar *pid_path;
    pid_t pid;

    error = NULL;
    credentials = g_credentials_new ();
    pid = g_credentials_get_unix_pid (credentials, &error);
    if (error)
    {
        LOG_error ("Couldn't get PID: %s", error->message);
        g_error_free (error);
        return false;
    }
    g_object_unref (credentials);

    pid_path = config_get_pid_path ();
    contents = g_strdup_printf ("%d", pid);
    file = g_file_new_for_path (pid_path);
    g_free (pid_path);
    iostream = g_file_create_readwrite (file, G_FILE_CREATE_PRIVATE,
        NULL, &error);
    if (error)
    {
        LOG_error ("the PID file couldn't be opened: %s", error->message);
        g_error_free (error);
        return false;
    }
    output = g_io_stream_get_output_stream (G_IO_STREAM (iostream));
    g_output_stream_write_all (output, contents, strlen(contents), NULL,
        NULL, &error);
    if (error)
    {
        LOG_error ("the PID file couldn't be written: %s", error->message);
        g_error_free (error);
        return false;
    }
    g_free (contents);
    g_output_stream_close (output, NULL, NULL);
    g_io_stream_close (G_IO_STREAM (iostream), NULL, NULL);
    g_object_unref (file);

    LOG_debug ("pid created");
    return true;
}

gchar *pid_read (void)
{
    GFile *file;
    GInputStream *input;
    GDataInputStream *dis;
    GError *error;
    gchar *contents;
    gchar *pid_path;

    error = NULL;
    pid_path = config_get_pid_path ();
    file = g_file_new_for_path (pid_path);
    g_free (pid_path);
    input = G_INPUT_STREAM (g_file_read (file, NULL, &error));
    if (error)
    {
        LOG_error ("the PID file couldn't be read: %s", error->message);
        g_error_free (error);
        return NULL;
    }
    dis = g_data_input_stream_new (input);
    contents = g_data_input_stream_read_line (dis, NULL, NULL, &error);
    if (error)
    {
        LOG_error ("the PID couldn't be read: %s", error->message);
        g_error_free (error);
        return NULL;
    }
    g_object_unref (file);
    g_object_unref (dis);
    g_input_stream_close (input, NULL, NULL);
    return contents;
}

void pid_delete (void)
{
    gchar *pid_path;
    GFile *file;

    pid_path = config_get_pid_path ();
    file = g_file_new_for_path (pid_path);
    g_file_delete (file, NULL, NULL);
    g_object_unref (file);
    g_free (pid_path);

    LOG_debug ("pid deleted");
}
