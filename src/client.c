#include <bson.h>
#include "log.h"
#include "server.h"
#include "protocol.h"
#include "database.h"
#include "room.h"
#include "client.h"

enum 
{
    SIGNAL_DESTROY
};

typedef struct {
    int signal;
    SkClient *self;
    gpointer user_data;
    GFunc func;
} _SignalData;

void client_free (SkClient *self)
{
    g_hook_list_invoke (&self->destroysignal, FALSE);
    g_hook_list_clear (&self->destroysignal);
    if (self->socket)
    {
        g_object_unref (self->socket);
        self->socket = NULL;
    }
    if (self->assd_id)
    {
        g_free (self->assd_id);
        self->assd_id = NULL;
    }
    LOG_debug ("clientfd %d closed", self->fd);
    g_free (self);
}

void client_send_message (SkClient *client, const bson_t *doc)
{
    gchar *to, *body;
    const SkClient *to_client;
    bson_iter_t iter;
    int rc;
    
    /* FIXME: pass 'e' */
    bson_iter_init_find (&iter, doc, "w");
    to = g_strdup (bson_iter_utf8 (&iter, NULL));
    rc = client_status (to, &to_client);
    if (rc == USER_ONLINE)
    {
        bson_iter_init_find (&iter, doc, "m");
        body = g_strdup (bson_iter_utf8 (&iter, NULL));
        protocol_send_message (to_client->fd, MESSAGE, 
            "f", DATA_STRING, client->assd_id, 
            "m", DATA_STRING, body,
            NULL);
        LOG_debug ("%s -> %s", client->assd_id, to_client->assd_id);
        g_free (body);
    }
    else if (rc == USER_OFFLINE)
    {
        bson_t *doc_to_write = bson_copy (doc);
        bson_append_utf8 (doc_to_write, "f", 1, client->assd_id, -1);
        database_log_now (to, doc_to_write);
        bson_destroy (doc_to_write);
        LOG_debug ("%s -> (offline) %s", client->assd_id, to);
    }
    else if (rc == USER_NONEXIST)
    {
        protocol_send_error (client->fd, ERROR_BAD_ID, "Bad ID given");
        LOG_debug ("%s -> (bad) %s", client->assd_id, to);
    }
    g_free (to);
}

SkClient *client_new_empty (int fd)
{
    SkClient *ins = g_malloc0 (sizeof (SkClient));
    ins->fd = fd;
    ins->status = STATUS_GOOFFLINE;
    ins->loggedin = false;
    ins->assd_id = NULL;
    ins->socket = g_socket_new_from_fd (ins->fd, NULL);
    g_hook_list_init (&ins->destroysignal, sizeof (GHook));
    return ins;
}

SkClient *client_new (int fd)
{
    char headerbuf;
    GError *error = NULL;
    SkClient *ins = client_new_empty (fd);
    /* read the protocol header */
    g_socket_set_timeout (ins->socket, 1);
    g_socket_receive (ins->socket, &headerbuf, sizeof(char), NULL, &error);
    if (error)
    {
        LOG_debug ("bad protocol from fd %d", ins->fd);
        client_free (ins);
        return NULL;
    }
    /* read client version */
    g_socket_receive (ins->socket, ins->version, sizeof(ins->version), NULL, &error);
    if (error)
    {
        LOG_debug ("bad version from fd %d", ins->fd);
        client_free (ins);
        return NULL;
    }
    if (protocol_version_supported (ins->version) != true)
    {
        LOG_debug ("client with bad version %X %X rejected", ins->version[0],
            ins->version[1]);
        protocol_send_error (ins->fd, ERROR_VERSION_UNSUPPORTED, 
            "This version is not supported");
        client_free (ins);
        return NULL;
    }
    g_socket_set_timeout (ins->socket, 0);
    return ins;
}

static void _signal_received (gpointer user_data)
{
    _SignalData *data = (_SignalData*)user_data;
    switch (data->signal)
    {
    case SIGNAL_DESTROY:
        data->func (data->self, data->user_data);
        break;
    default:
        g_assert_not_reached ();
        break;
    }
}

static void _signal_destroyed (gpointer data)
{
    GHook *hook = (GHook *)data;
    g_free (hook->data);
}

void client_connect_destroy (SkClient *client, 
    GFunc func, gpointer user_data)
{
    GHook *hook = g_hook_alloc (&client->destroysignal);
    _SignalData *data = g_malloc0 (sizeof (_SignalData));
    data->signal = SIGNAL_DESTROY;
    data->self = client;
    data->user_data = user_data;
    data->func = func;
    hook->func = _signal_received;
    hook->destroy = _signal_destroyed;
    hook->data = data;
    g_hook_append (&client->destroysignal, hook);
}

void client_message_handle (SkClient *client, const bson_t *doc)
{
    bson_iter_t iter;
    SkMessageType type;
    bson_iter_init_find (&iter, doc, "t");
    type = bson_iter_int32 (&iter);
    switch (type)
    {
        /* state */
        char *id = NULL, *password = NULL;
        
    case NEW_ACCOUNT:
    {
        char *id = NULL, *password = NULL, *nick = NULL;
        GError *error = NULL;
        
        bson_iter_init_find (&iter, doc, "u");
        id = g_strdup (bson_iter_utf8 (&iter, NULL));
        bson_iter_init_find (&iter, doc, "p");
        password = g_strdup (bson_iter_utf8 (&iter, NULL));
        bson_iter_init_find (&iter, doc, "n");
        nick = g_strdup (bson_iter_utf8 (&iter, NULL));
        error = NULL;
        if (user_mgmt_lookup (id) != NULL)
        {
            protocol_send_error (client->fd, ERROR_ID_TAKEN, "This ID has been"
                " used by the internal server");
            break;
        }
        if (!database_new_account (id, nick, password, &error))
        {
            protocol_send_error (client->fd, error->code, error->message);
            g_error_free (error);
        }
        else 
        {
            protocol_send_message (client->fd, NEW_ACCOUNT, NULL);
        }
        g_free (id);
        g_free (password);
        g_free (nick);
    }
        break;
    case JOIN_ROOM:
    {
        const gchar *roomname;
        
        bson_iter_init_find (&iter, doc, "n");
        roomname = bson_iter_utf8 (&iter, NULL);
        if (room_join (roomname, client))
            goto lbl_state_confirm;
        else 
            protocol_send_error (client->fd, ERROR_INTERNAL, "Internal errors, "
                "please wait a moment");
    }
        break;
    case QUIT_ROOM:
    {
        const gchar *roomname;
        
        bson_iter_init_find (&iter, doc, "n");
        roomname = bson_iter_utf8 (&iter, NULL);
        if (room_quit (roomname, client))
            goto lbl_state_confirm;
        else 
            protocol_send_error (client->fd, ERROR_INTERNAL, "Internal errors, "
                "please wait a moment");
    }
        break;
    case STATUS_GOOFFLINE:
        user_mgmt_offline (client);
        goto lbl_state_confirm;
    case STATUS_GOONLINE:
    {
        bson_iter_init_find (&iter, doc, "u");
        id = g_strdup (bson_iter_utf8 (&iter, NULL));
        bson_iter_init_find (&iter, doc, "p");
        password = g_strdup (bson_iter_utf8 (&iter, NULL));
        if (database_auth (id, password, client) == false)
        {
            protocol_send_error (client->fd, ERROR_AUTH_FAILED, 
                "Authentication failed");
            LOG_message ("clientfd %d failed to auth as %s", client->fd, id);
            g_free (id);
            g_free (password);
            break;
        }
        LOG_message ("%s (clientfd %d) successfully auth'd", id, client->fd);
        client->assd_id = id;
        user_mgmt_new_online (client);
        g_free (password);
        /* check for offline messages and send back */
        GList *offlinemsg = database_get_logs (id);
        while (offlinemsg)
        {
            GError *error = NULL;
            bson_t *doc = (bson_t *)offlinemsg->data;
            g_socket_send (client->socket, (const gchar *)bson_get_data (doc), 
                doc->len, NULL, &error);
            if (error)
            {
                LOG_critical ("Write to %s@%d error: %s", client->assd_id, 
                    client->fd, error->message);
                g_error_free (error);
            }
            offlinemsg = offlinemsg->next;
        }
        g_list_free_full (offlinemsg, (GDestroyNotify)bson_destroy);
        goto lbl_state_confirm;
    }
    case STATUS_GOAWAY:
    case STATUS_GOBUSY:
    case STATUS_GOINVISIBLE:
    case STATUS_GOTALKTOME:
    lbl_state_confirm:
        protocol_confirm_change_status (client->fd, type);
        break;
    case MESSAGE:
        client_send_message (client, doc);
        break;
    default:
        /* FIXME: handle other types */
        break;
    }
}

SkUserState client_status (const char *id, const SkClient ** client)
{
    bool if_register;
    if_register = database_if_register (id);
    *client = user_mgmt_lookup (id);
    if (*client != NULL)
        return USER_ONLINE;
    else if (*client == NULL && if_register == TRUE)
        return USER_OFFLINE;
    else if (*client == NULL && if_register != TRUE)
        return USER_NONEXIST;
    g_assert_not_reached ();
    return USER_NONEXIST;
}
