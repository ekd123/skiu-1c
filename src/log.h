#pragma once

/**
 * LOG_message:
 * 
 * Print a message.
 */
#define LOG_message g_message
/**
 * LOG_error:
 * 
 * Print a fault error and abort the program.
 **/
#define LOG_error g_error
/**
 * LOG_warning:
 * 
 * Print a warning.
 **/
#define LOG_warning g_warning
/**
 * LOG_debug:
 * 
 * Print a message for debugging.
 **/
#define LOG_debug g_debug
/**
 * LOG_critical:
 * 
 * Print a critical error.
 **/
#define LOG_critical g_critical

/**
 * log_init:
 * 
 * Initialize the logging system.
 */
void log_init (void);

/**
 * log_uninit:
 * 
 * Uninitialize the logging system.
 */
void log_uninit (void);
