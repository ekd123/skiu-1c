#include <glib.h>
#include <libpq-fe.h>
#include "random.h"
#include "log.h"
#include "config.h"
#include "database.h"
#include "database-pgsql.h"

#include <sys/stat.h>
#include <fcntl.h>

struct pgpriv {
    PGconn *conn;
};

#define ERROR_QUARK g_quark_from_static_string("database-pgsql")

static void _connect (idatabase *i)
{
    struct pgpriv *priv = (struct pgpriv *)i->extra_data;
    gchar *conninfo = config_get_pgsql_conninfo ();
    PGresult *res;

    priv->conn = PQconnectdb (conninfo);
    if (PQstatus (priv->conn) != CONNECTION_OK)
    {
        LOG_error ("Database connecting: %s", PQerrorMessage (priv->conn));
        g_free (conninfo);
        abort ();
    }
    
    res = PQexec (priv->conn, "CREATE TABLE users (id TEXT PRIMARY KEY, "
        "nickname text, password TEXT, salt VARCHAR(10))");
    PQclear (res);
    res = PQexec (priv->conn, "CREATE TABLE logs (toid TEXT, doc BYTEA, "
        "date INT)");
    PQclear (res);
    g_free (conninfo);
}

static void _disconnect (idatabase *i)
{
    struct pgpriv *priv = (struct pgpriv *)i->extra_data;
    PQfinish (priv->conn);
}

static void _free (idatabase *i)
{
    g_free (i->extra_data);
    g_free (i);
}

static bool _auth (idatabase *i, const char *id, const char *password, 
    SkClient *client)
{
    struct pgpriv *priv = (struct pgpriv *)i->extra_data;
    char *sql = NULL;
    bool matched = false;
    PGresult *res;
    char *dbsalt, *dbchecksum;
    gchar *computed_checksum, *combined = NULL;
    
    if (database_valid (id) != true)
        return false;
    sql = g_strdup_printf ("SELECT * FROM users WHERE id='%s'", id);
    res = PQexec (priv->conn, sql);
    if (PQntuples (res) != 1)
    {
        goto lbl_cleanup;
    }
    dbsalt = PQgetvalue (res, 0, 3);
    dbchecksum = PQgetvalue (res, 0, 2);
    combined = g_strdup_printf ("%s%s%s", dbsalt, password, dbsalt);
    computed_checksum = g_compute_checksum_for_string (G_CHECKSUM_SHA512, 
        combined, -1);
    if (!g_strcmp0 (computed_checksum, dbchecksum))
        matched = true;
  lbl_cleanup:
    PQclear (res);
    g_free (sql);
    if (combined)
        g_free (combined);
    return matched;
}

static SkErrorNumber 
_register (idatabase *i, const char *id, const char *nickname, 
    const char *password, GError **error)
{
    if (database_if_register (id) == true)
    {
        g_set_error_literal (error, ERROR_QUARK, ERROR_ID_TAKEN, 
            "This ID has already been taken");
        return false;
    }
    
    bool success = true;
    struct pgpriv *priv = (struct pgpriv *)i->extra_data;
    gchar *salt = random_generate_salt (10);
    gchar *combined = g_strdup_printf ("%s%s%s", salt, password, salt);
    gchar *checksum = g_compute_checksum_for_string (G_CHECKSUM_SHA512, 
        combined, -1);
    gchar *sql = g_strdup_printf ("INSERT INTO users VALUES ('%s', '%s', "
        "'%s', '%s')", id, nickname, checksum, salt);
    PGresult *res;
    res = PQexec (priv->conn, sql);
    if (PQstatus (priv->conn) != CONNECTION_OK)
    {
        LOG_critical ("Couldn't insert into users: %s", 
            PQerrorMessage (priv->conn));
        success = false;
    }
    PQclear (res);
    g_free (salt);
    g_free (combined);
    g_free (checksum);
    g_free (sql);
    if (success)
    {
        LOG_message ("new account ID: %s, Nick: %s", id, nickname);
    }
    return success;
}

static bool _if_register (idatabase *i, const char *id)
{
    struct pgpriv *priv = (struct pgpriv *)i->extra_data;
    char *sql = NULL;
    bool found = false;
    PGresult *res;
    
    if (database_valid (id) != true)
        return false;
    sql = g_strdup_printf ("SELECT * FROM users WHERE id='%s'", id);
    res = PQexec (priv->conn, sql);
    if (PQntuples (res) == 1)
        found = true;
    else
        found = false; /* no duplicates */
    PQclear (res);
    g_free (sql);
    return found;
}

bool _log (idatabase *i,const char *toid, const bson_t *doc, GDateTime *utctime)
{
    bool success = true;
    struct pgpriv *priv = (struct pgpriv *)i->extra_data;
    PGresult *res;
    char *sql = g_strdup_printf ("INSERT INTO logs VALUES ($1::text, $2::bytea,"
        "$3::bigint)");
    gint64 unixtime = GINT64_TO_BE (g_date_time_to_unix (utctime));
    const void *data = (void *)bson_get_data (doc);
    const char* const paramValues[] = { toid, data, (char *)&unixtime };
    int paramLengths[] = { strlen (toid), doc->len, sizeof (unixtime) };
    int paramFormats[] = { 0, 1, 1 };
    
    res = PQexecParams (priv->conn, sql, 3, NULL, paramValues, 
        paramLengths, paramFormats, 0);
    if (PQresultStatus (res) != PGRES_COMMAND_OK)
    {
        LOG_critical ("Couldn't insert into logs: %s", 
            PQerrorMessage (priv->conn));
        success = false;
    }
    
    g_free (sql);
    PQclear (res);
    return success;
}

static GList *_get_logs (struct idatabase *i, const char *toid)
{
    GList *list = NULL;
    struct pgpriv *priv = (struct pgpriv *)i->extra_data;
    char *sql;
    PGresult *res;
    
    /* Read messages */
    sql  = g_strdup_printf ("SELECT * FROM logs WHERE toid='%s'", toid);
    res = PQexec (priv->conn, sql);
    g_free (sql);
    for (int tuple = 0; tuple < PQntuples (res); tuple++)
    {
        size_t len;
        const char *odoc_escaped = PQgetvalue (res, tuple, 1);
        void *odoc = (void *)PQunescapeBytea ((guchar *)odoc_escaped, &len);
        const char *unixtime = PQgetvalue (res, tuple, 2);
        bson_t *doc = bson_new_from_data (odoc, len);
        free (odoc);
        
        bson_append_time_t (doc, "s", 1, atoi (unixtime));
        list = g_list_append (list, doc);
    }
    PQclear (res);
    
    /* Delete messages */
    sql = g_strdup_printf ("DELETE FROM logs WHERE toid='%s'", toid);
    res = PQexec (priv->conn, sql);
    PQclear (res);
    g_free (sql);
    return list;
}

idatabase *database_pgsql_new (void)
{
    struct pgpriv *priv = g_malloc0 (sizeof (struct pgpriv));
    idatabase *i = (idatabase*)g_malloc0 (sizeof (idatabase));
    i->free = _free;
    i->dbconnect = _connect;
    i->dbdisconnect = _disconnect;
    i->auth = _auth;
    i->new_account = _register;
    i->if_register = _if_register;
    i->log = _log;
    i->get_logs = _get_logs;
    i->extra_data = priv;
    return i;
}