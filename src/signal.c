#include <stdlib.h>
#include </usr/include/signal.h> // wtf
#include <glib.h>
#include "log.h"
#include "common.h"
#include "signal.h"

typedef void (*sighandler_t)(int);
static sighandler_t origint = NULL;
static sighandler_t origterm = NULL;
static sighandler_t origpipe = NULL;

static void sighandler_int_term (int signo)
{
    LOG_debug ("signal %d caught", signo);
    exit (0);
}

static sighandler_t _setignore (int signo)
{
    return signal (signo, SIG_IGN);
}

CONSTRUCTOR(110)
void signal_init (void)
{
    origint = signal (SIGINT, sighandler_int_term);
    origterm = signal (SIGTERM, sighandler_int_term);
    origpipe = _setignore (SIGPIPE);
    LOG_debug ("signal init'd");
}

DESTRUCTOR(110)
void signal_uninit (void)
{
    signal (SIGINT, origint);
    signal (SIGTERM, origterm);
    signal (SIGPIPE, origpipe);
    LOG_debug ("signal uninit'd");
}