#include <glib.h>
#include "common.h"
#include "user.h"
#include "log.h"
#include "server.h"
#include "room.h"

/**
 * key: roomname
 * val: GSList of SkClient
 */
static GHashTable *rooms = NULL;

CONSTRUCTOR(150)
void room_init (void)
{
    rooms = g_hash_table_new (g_str_hash, g_str_equal);
    LOG_debug ("room init'd");
}

DESTRUCTOR(150)
void room_uninit (void)
{
    g_hash_table_unref (rooms);
    LOG_debug ("room uninit'd");
}

gboolean _on_message (GIOChannel *source, GIOCondition condition, 
    SkClient *roomclient)
{
    extern void _send_cb (gpointer data, gpointer user_data);
    bson_t *doc;
    bson_reader_t reader;
    GDateTime *dt = g_date_time_new_now_utc ();
    const bson_t *odoc;
    bson_bool_t reached_eof;
    GSList *list; /* do not free */
    
    bson_reader_init_from_fd (&reader, g_io_channel_unix_get_fd(source), false);
    odoc = bson_reader_read (&reader, &reached_eof);
    doc = bson_copy (odoc);
    bson_append_utf8 (doc, "r", 1, roomclient->assd_id, -1);
    bson_append_time_t (doc, "s", 1, g_date_time_to_unix (dt));
    list = g_hash_table_lookup (rooms, roomclient->assd_id);
    g_slist_foreach (list, _send_cb, doc);
    
    g_date_time_unref (dt);
    bson_reader_destroy (&reader);
    bson_destroy (doc);
    return TRUE;
    /* return FALSE; // to remove event source */
}

void _room_new (const gchar *roomname)
{
    SkClient *client = user_map_new ((GIOFunc)_on_message);
    client->assd_id = g_strdup (roomname);
    user_mgmt_new_online (client);
    server_listener_add (client);
    LOG_debug ("new room %s created", roomname);
}

bool room_join (const gchar *roomname, const SkClient *client)
{
    GSList *list = NULL;
    
    /* Create a room if it doesn't exist */
    if (!g_hash_table_lookup_extended (rooms, roomname, NULL, (gpointer*)&list))
    {
        _room_new (roomname);
    }
    /* Add user */
    list = g_slist_append (list, (gpointer)client);
    g_hash_table_insert (rooms, (gpointer)roomname, list);
    return false;
}

bool room_quit (const gchar *roomname, const SkClient *client)
{
    GSList *list = g_hash_table_lookup (rooms, roomname);
    list = g_slist_remove (list, client);
    g_hash_table_insert (rooms, (gpointer)roomname, list);
    return false;
}
