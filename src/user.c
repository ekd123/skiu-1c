#include <sys/socket.h>
#include <errno.h>
#include <glib.h>
#include "common.h"
#include "server.h"
#include "log.h"
#include "user.h"

static GHashTable *online_users = NULL;

CONSTRUCTOR(130)
void user_mgmt_init (void)
{
    online_users = g_hash_table_new (g_str_hash, g_str_equal);
    LOG_debug ("user mgmt init'd");
}

DESTRUCTOR(130)
void user_mgmt_uninit (void)
{
    g_hash_table_destroy (online_users);
    LOG_debug ("user mgmt uninit'd");
}

void user_mgmt_new_online (SkClient *client)
{
    if (!client)
        return;
    g_hash_table_insert (online_users, client->assd_id, client);
    LOG_debug ("%s@%d now online", client->assd_id, client->fd);
}

void user_mgmt_offline (SkClient *client)
{
    if (!client || !client->assd_id)
        return;
    /* FIXME: this doesn't know if user exists */
    g_hash_table_remove (online_users, client->assd_id);
    LOG_debug ("%s@%d now offline", client->assd_id, client->fd);
}

const SkClient *user_mgmt_lookup (const char *id)
{
    g_return_val_if_fail (id != NULL, NULL);
    return (SkClient *)g_hash_table_lookup (online_users, id);
}

SkClient *user_map_new (GIOFunc read_ready)
{
    /**
     * fds[0] is in the SkClient object. where new messages goes
     * fds[1] is what we listen on
     */
    int fds[2] = { 0 };
    GIOChannel *channel = NULL;
    SkClient *client = NULL;
    
    if (socketpair (AF_LOCAL, SOCK_STREAM, 0, fds))
    {
        char *errmsg = strerror (errno);
        LOG_critical ("Couldn't socketpair: %s", errmsg);
        free (errmsg);
        return NULL;
    }
    client = client_new_empty (fds[0]);
    channel = g_io_channel_unix_new (fds[1]);
    g_io_add_watch (channel, G_IO_IN|G_IO_PRI|G_IO_ERR|G_IO_HUP, read_ready, 
        client);
    return client;
}
