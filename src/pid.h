#pragma once

#include "config.h"
#include <stdbool.h>

/**
 * pid_exists:
 * 
 * Return value: false if the pid file already exists,
 * true if the pid file doesn't exist.
 **/
bool pid_exists (void);

/**
 * pid_create:
 * 
 * Creates a pid file with path PID_FILE,
 * writes the PID of current process to the file, 
 * aborts if there are any errors.
 * 
 * Return value: true on success, otherwise false.
 **/
bool pid_create (void);

/**
 * pid_read:
 * 
 * Opens the exisiting pid file.
 * and reads the contents.
 * 
 * Return value: (transfer-full): the pid in string
 **/
gchar *pid_read (void);

/**
 * pid_delete:
 * 
 * Removes the pid file
 */
void pid_delete (void);

