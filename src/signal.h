#pragma once 

/**
 * This part covers signal handling.
 * 
 * Exit normally when the server receives SIGTERM or SIGINT.
 */

/**
 * signal_init:
 * 
 * register all the default handlers
 **/
void signal_init (void);

/**
 * signal_uninit:
 * 
 * unregister all the default handlers
 **/
void signal_uninit (void);

