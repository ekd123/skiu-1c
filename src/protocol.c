#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <bson.h>
#include <glib.h>
#include "log.h"
#include "protocol.h"

static char supported_versions[][2] = {
    { 0x1E, 0x0D }
};

bool protocol_version_supported (char version[2])
{
    int i;
    int versnumber = sizeof(supported_versions)/sizeof(supported_versions[0]);
    for (i = 0; i < versnumber; i++)
    {
        if (strncmp (version, supported_versions[i], 2) == 0)
        {
            return true;
        }
    }
    return false;
}

static bool protocol_send_message_valist (int fd, SkMessageType mesgtype, 
    const char *first_key, va_list var_args)
{
    bson_t *doc;
    const char *key;
    const bson_uint8_t *data;
    bool result;
    
    result = false;
    doc = bson_new ();
    
    bson_append_int32 (doc, "t", 1, mesgtype);
    if (first_key != NULL)
    {
        key = first_key;
        do
        {
            const char *vstring;
            int32_t vint32;
            int64_t vint64;
            double vdouble;
            bson_bool_t vbool;
            SkProtocolDataType datype;
            
            datype = va_arg (var_args, SkProtocolDataType);
            switch (datype)
            {
            case DATA_STRING:
                vstring = va_arg (var_args, const char *);
                result = bson_append_utf8 (doc, key, 1, vstring, -1);
                break;
            case DATA_INT32:
                vint32 = va_arg (var_args, int32_t);
                result = bson_append_int32 (doc, key, 1, vint32);
                break;
            case DATA_INT64:
                vint64 = va_arg (var_args, int64_t);
                result = bson_append_int64 (doc, key, 1, vint64);
                break;
            case DATA_DOUBLE:
                vdouble = va_arg (var_args, double);
                result = bson_append_double (doc, key, 1, vdouble);
                break;
            case DATA_BOOL:
                vbool = va_arg (var_args, bson_bool_t);
                result = bson_append_bool (doc, key, 1, vbool);
                break;
            default:
                LOG_critical ("an unknown data type got");
                goto cleanup;
                break;
            }
        } while ((key = va_arg (var_args, const char *)));
    }
    data = bson_get_data (doc);
    if (write (fd, data, doc->len) != doc->len)
    {
        LOG_warning ("data sent to clientfd %d is incomplete", fd);
    }
  cleanup:
    bson_destroy (doc);
    return result;
}

bool protocol_send_message (int fd, SkMessageType mesgtype, const char *key, 
    ...)
{
    bool result;
    va_list var_args;
    va_start (var_args, key);
    result = protocol_send_message_valist (fd, mesgtype, key, var_args);
    va_end (var_args);
    return result;
}

bool protocol_send_error (int fd, SkErrorNumber err, const char *desc)
{
    return protocol_send_message (fd, ERROR, 
        "n", DATA_INT32, err, 
        "d", DATA_STRING, desc, 
        NULL);
}

bool protocol_confirm_change_status (int fd, SkMessageType state)
{
    LOG_debug ("clientfd %d changed status to %d", fd, state);
    return protocol_send_message (fd, state, NULL);
}

