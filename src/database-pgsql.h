#pragma once

#include "database.h"

/**
 * database_pgsql_new:
 * Return value: (transfer-full): a new #idatabase for PostgreSQL.
 */
idatabase *database_pgsql_new (void);
