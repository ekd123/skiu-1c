#pragma once

#include <sys/types.h>
#include <stdbool.h>

/**
 * SkMessageType:
 * 
 * Type of a BSON document.
 * 
 * @INVALID: an invalid message
 * @NEW_ACCOUNT: create a new account on this server
 * @MONITOR: change notifications
 * @STATUS_GOONLINE: change state to online
 * @STATUS_GOAWAY: change state to away
 * @STATUS_GOBUSY: change state to busy
 * @STATUS_GOTALKTOME: change state to talk-to-me
 * @STATUS_GOINVISIBLE: change state to invisible
 * @STATUS_BAD: can't change state
 * @MESSAGE: a message
 * @ERROR: error report
 */
typedef enum {
    INVALID = 0, 
    NEW_ACCOUNT = 1, 
    MONITOR = 2, 
    
    JOIN_ROOM = 10, 
    QUIT_ROOM = 11, 
    
    STATUS_GOONLINE = 100, 
    STATUS_GOAWAY, 
    STATUS_GOBUSY, 
    STATUS_GOTALKTOME, 
    STATUS_GOINVISIBLE, 
    STATUS_GOOFFLINE, 
    
    STATUS_BAD, 
    
    MESSAGE = 400, 
    
    ERROR = 500,
} SkMessageType;

/**
 * SkErrorNumber:
 * @ERROR_VERSION_UNSUPPORTED: an unsupported version
 * @ERROR_AUTH_FAILED: bad username/password combination
 * @ERROR_BAD_ID: bad ID given, ie. this user doesn't exist
 * @ERROR_SERVER_QUIT: server has quited
 * @ERROR_INTERNAL: internal errors, couldn't recover automatically
 * @ERROR_ID_TAKEN: this ID has already been taken by others
 * 
 * Error number to pass to clients. The 'n' field in ERROR.
 */
typedef enum {
    ERROR_VERSION_UNSUPPORTED = 100,
    ERROR_AUTH_FAILED,
    ERROR_BAD_ID,
    ERROR_SERVER_QUIT,
    
    ERROR_INTERNAL,
    ERROR_ID_TAKEN,
} SkErrorNumber;

/**
 * SkAccountType:
 * @ACCOUNT_GROUP: a group.
 * @ACCOUNT_ROOM: a room.
 * @ACCOUNT_USER: a user
 */
typedef enum {
    ACCOUNT_GROUP,
    ACCOUNT_ROOM,
    ACCOUNT_USER
} SkAccountType;

/**
 * SkProtocolDataType:
 * @DATA_STRING:
 * @DATA_INT32:
 * @DATA_INT64:
 * @DATA_BINARY:
 * @DATA_BOOL:
 * @DATA_DOUBLE:
 */
typedef enum {
    DATA_STRING,
    DATA_INT32,
    DATA_INT64,
    DATA_BINARY,
    DATA_BOOL,
    DATA_DOUBLE
} SkProtocolDataType;

/**
 * SkBinaryChunk:
 * @bytes: the raw data
 * @length: length of @data
 * 
 * This structure represents a binary chunk. Needed for DATA_BINARY.
 */
typedef struct {
    void *bytes;
    size_t length;
} SkBinaryChunk;

/**
 * protocol_version_supported:
 * @version: the version to be checked
 * 
 * Check if the version is supported by the server.
 * 
 * Return value: true if supported.
 */
bool protocol_version_supported(char version[2]);
/**
 * protocol_send_message:
 * @fd: fd
 * @mesgtype: message type
 * @key: the first key
 * @...: a list ended with NULL
 * 
 * send a BSON document to @fd.
 * 
 * Return value: true on success.
 **/
bool protocol_send_message (int fd, SkMessageType mesgtype, const char *key, 
    ...);

/**
 * protocol_send_error:
 * @fd: fd
 * @err: the errno
 * @desc: description of the error
 * 
 * Send an error.
 * 
 * Return value: true on success.
 **/
bool protocol_send_error (int fd, SkErrorNumber err, const char *desc);

/**
 * protocol_confirm_change_status:
 * @fd: fd
 * @state: which state to be confirmed
 * 
 * Confirm client's state-changing.
 * 
 * Return value: true on success.
 **/
bool protocol_confirm_change_status (int fd, SkMessageType state);
