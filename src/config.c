#include "log.h"
#include "common.h"
#include <gio/gio.h>

static GKeyFile *keyfile;

CONSTRUCTOR(105)
void config_init (void)
{
    GError *error = NULL;
    keyfile = g_key_file_new ();
    (void)g_key_file_load_from_data_dirs (keyfile, 
        "skiu/skiu.cfg", 
        NULL, 
        G_KEY_FILE_NONE, 
        &error);
    LOG_debug ("config init'd");
}

DESTRUCTOR(105)
void config_uninit (void)
{
    g_key_file_unref (keyfile);
    LOG_debug ("config uninit'd");
}

#define GET_STR(id,default) \
gchar *config_get_ ##id (void) { \
    gchar *t; \
    return (t=g_key_file_get_string(keyfile, "server", #id, NULL))?t:g_strdup(default); \
}
#define GET_INT(id,default) \
gint config_get_ ##id (void) { \
    if (g_key_file_has_key (keyfile, "server", #id, NULL)) { \
        return g_key_file_get_integer (keyfile, "server", #id, NULL); \
    } else { \
        return default; \
    } \
}

GET_STR(preferred_db, "postgresql")
GET_STR(pgsql_conninfo, "hostaddr=127.0.0.1")
GET_STR(sqlite_path, "data.db")
GET_STR(pid_path, "/var/run/skiu.pid")

GET_INT(port, 12345)
#undef GET_STR
#undef GET_INT
